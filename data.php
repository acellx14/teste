<?php $page = 'home';include 'header.php'; 
 //entry.php
 $connect = mysqli_connect("localhost", "root", "", "test");    
 session_start();  
 if(!isset($_SESSION["username"]))  
 {  
      header("location:index.php?action=login");  
 }
 function get_all_data($connect){
    $get_data = mysqli_query($connect,"SELECT * FROM `test_data`");
    if(mysqli_num_rows($get_data) > 0){
        echo '<table>
              <tr>
                <th>Nome</th>
                <th>Descrição</th> 
                <th>Action</th> 
              </tr>';
        while($row = mysqli_fetch_assoc($get_data)){
           
            echo '<tr>
            <td>'.$row['nome'].'</td>
            <td>'.$row['descricao'].'</td>
            <td>
            <a href="update.php?id='.$row['id'].'">Edit</a> |
            <a href="delete.php?id='.$row['id'].'">Delete</a>
            </td>
            </tr>';

        }
        echo '</table>';
    }else{
        echo "<h3>No records found. Please insert some records</h3>";
    }
}  
 ?>  
 <!DOCTYPE html>  
 <html>  
      <head>  
           <title>Teste</title>  
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
           <link rel="stylesheet" href="style.css" />
           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
      </head>  
      <body>  
          <br />
          <br />
          <br />
          <div class="container">
               <!-- INSERT DATA -->
               <div class="form">
                    <h2>Insert Data</h2>
                    <form action="insert.php" method="post">
                         <strong>Nome</strong><br>
                         <input type="text" name="nome" placeholder="Enter your name" required><br>
                         <strong>Descrição</strong><br>
                         <input type="text" name="descricao" placeholder="Enter your description" required><br>
                         <input type="submit" value="Insert">
                    </form>
               </div>
               <br />
               <br />
               <!-- END OF INSERT DATA SECTION -->
               <hr>
               <!-- SHOW DATA -->
               <h2>Show Data</h2>
               <?php 
               // calling get_all_data function
               get_all_data($connect); 
               ?>
               <!-- END OF SHOW DATA SECTION -->
          </div>
          <script src="bootstrap.min.js"></script>  
      </body>  
 </html>  