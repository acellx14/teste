<?php
require 'db_connection.php';

if(isset($_POST['nome']) && isset($_POST['descricao'])){
    
    // check username and email empty or not
    if(!empty($_POST['nome']) && !empty($_POST['descricao'])){
        
        // Escape special characters.
        $nome = mysqli_real_escape_string($conn, htmlspecialchars($_POST['nome']));
        $descricao = mysqli_real_escape_string($conn, htmlspecialchars($_POST['descricao']));
                
                // INSERT DATA INTO THE DATABASE
                $insert_query = mysqli_query($conn,"INSERT INTO `test_data`(nome,descricao) VALUES('$nome','$descricao')");

                //CHECK DATA INSERTED OR NOT
                if($insert_query){
                    echo "<script>
                    alert('Data inserted');
                    window.location.href = 'data.php';
                    </script>";
                    exit;
                }else{
                    echo "<h3>Opps something wrong!</h3>";
                }
                
        
    }else{
        echo "<h4>Please fill all fields</h4>";
    }
    
}else{
    // set header response code
    http_response_code(404);
    echo "<h1>404 Page Not Found!</h1>";
}
?>