<?php
require 'db_connection.php';
if(isset($_GET['id']) && is_numeric($_GET['id'])){
    
    $dataid = $_GET['id'];
    $get_data = mysqli_query($conn,"SELECT * FROM `test_data` WHERE id='$dataid'");
    
    if(mysqli_num_rows($get_data) === 1){
        
        $row = mysqli_fetch_assoc($get_data);
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update data</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
     <div class="container">
      
       <!-- UPDATE DATA -->
        <div class="form">
            <h2>Update Data</h2>
            <form action="" method="post">
                <strong>Nome</strong><br>
                <input type="text" autocomplete="off" name="nome" placeholder="Enter your full name" value="<?php echo $row['nome'];?>" required><br>
                <strong>Descrição</strong><br>
                <input type="text" autocomplete="off" name="descricao" placeholder="Enter your description" value="<?php echo $row['descricao'];?>" required><br>
                <input type="submit" value="Update">
            </form>
        </div>
        <!-- END OF UPDATE DATA SECTION -->
    </div>
</body>
</html>
<?php

    }else{
        // set header response code
        http_response_code(404);
        echo "<h1>404 Page Not Found!</h1>";
    }
    
}else{
    // set header response code
    http_response_code(404);
    echo "<h1>404 Page Not Found!</h1>";
}


/* ---------------------------------------------------------------------------
------------------------------------------------------------------------------ */


// UPDATING DATA

if(isset($_POST['nome']) && isset($_POST['descricao'])){
    
    // check name and description empty or not
    if(!empty($_POST['nome']) && !empty($_POST['descricao'])){
        
        // Escape special characters.
        $nome = mysqli_real_escape_string($conn, htmlspecialchars($_POST['nome']));
        $descricao = mysqli_real_escape_string($conn, htmlspecialchars($_POST['descricao']));
        
            $data_id = $_GET['id'];
                
                // UPDATE DATA               
                $update_query = mysqli_query($conn,"UPDATE `test_data` SET nome='$nome',descricao='$descricao' WHERE id=$data_id");

                //CHECK DATA UPDATED OR NOT
                if($update_query){
                    echo "<script>
                    alert('Data Updated');
                    window.location.href = 'data.php';
                    </script>";
                    exit;
                }else{
                    echo "<h3>Opps something wrong!</h3>";
                }
        
    }else{
        echo "<h4>Please fill all fields</h4>";
    }   
}
// END OF UPDATING DATA

?>